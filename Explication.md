# Maquette SolidWorks

Nous avons choisi d'utiliser 4 capteurs à ultrason et de les placer face à face deux à deux afin d'avoir 2 mesures, une sur l'axe des absices et l'autre sur les ordonnées. 

Nous avons aussi choisi de faire un rebond avec l'onde, dans le but d'optimiser l'espace. En effet, les capteurs doivent être suffisamenet éloigner pour être précis mais nous ne voulons pas que la station microclimat soit trop grande. Ce rebond permet tout cela.